require('dotenv/config');
const { exec } = require('child_process');
const fs = require('fs-extra');
const path = require('path');
const { objectMultiToOneDepth, objectOneToMultiDepth, sortObjectBykey } = require('./helpers');

console.log(path, path, fs);

const baseLang = process.env.REACT_APP_LOCALE_DEFAULT || 'vi';
const dir = path.resolve('src/res/translations');
const fileDefault = 'translations.json';
const folders = fs.readdirSync(dir).filter((langFolder) => {
  return fs.lstatSync(path.resolve(dir, langFolder)).isDirectory();
});

if (!folders.includes(baseLang)) {
  fs.mkdirSync(path.resolve(dir, baseLang));
  folders.unshift(baseLang);
}

console.log('Languages', folders);

let files = [];
folders.map((folder) => {
  const filesTemp = fs.readdirSync(path.resolve(dir, folder)).filter((file) => {
    return fs.lstatSync(path.resolve(dir, folder, file)).isFile();
  });
  files = [...files, ...filesTemp]
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort();
});
console.log('Files', files);

if (!files.length) {
  files.push(fileDefault);
}

files.map((file) => {
  let baseContentJson = {};
  try {
    console.log(`Rewrite ${baseLang}/${file}`);
    if (fs.existsSync(path.resolve(dir, baseLang, file))) {
      const contentText = fs.readFileSync(path.resolve(dir, baseLang, file), 'utf-8');
      baseContentJson = JSON.parse(contentText);
    } else {
      fs.writeFileSync(path.resolve(dir, baseLang, file), '{}');
    }
    baseContentJson = objectMultiToOneDepth(baseContentJson);
    fs.writeFileSync(
      path.resolve(dir, baseLang, file),
      JSON.stringify(sortObjectBykey(objectOneToMultiDepth(baseContentJson)))
    );
  } catch (error) {
    console.log(`Rewrite ${baseLang}/${file} fail`);
  }
  folders
    .filter((x) => x !== baseLang)
    .map((folder) => {
      try {
        console.log(`Sync ${folder}/${file}`);
        if (!fs.existsSync(path.resolve(dir, folder, file))) {
          fs.writeFileSync(path.resolve(dir, folder, file), '{}');
        }
        const contentText = fs.readFileSync(path.resolve(dir, folder, file), 'utf-8');
        let contentJsonTemp;
        if (contentText === '') {
          contentJsonTemp = {};
        } else {
          contentJsonTemp = JSON.parse(contentText);
        }
        contentJsonTemp = objectMultiToOneDepth(contentJsonTemp);
        let writeContentJson = { ...baseContentJson };
        for (const key in contentJsonTemp) {
          writeContentJson[key] = contentJsonTemp[key];
        }
        console.log(writeContentJson);
        fs.writeFileSync(
          path.resolve(dir, folder, file),
          JSON.stringify(sortObjectBykey(objectOneToMultiDepth(writeContentJson)))
        );
      } catch (error) {
        console.log(`Sync ${folder}/${file} fail`);
      }
    });
});

console.log('formatting...');
exec('yarn format:lang');
