require('dotenv/config');
const path = require('path');
const AntDesignThemePlugin = require('antd-theme-webpack-plugin');
const { getLessVars } = require('antd-theme-generator');
const CracoLessPlugin = require('craco-less');

const themeVariables = getLessVars('./src/themes/styles/variables.less');
const defaultVars = {
  ...getLessVars('./node_modules/antd/es/style/themes/default.less'),
  ...themeVariables
};

module.exports = {
  devServer: {
    proxy: {
      '/api': process.env.REACT_APP_PROXY_API || ''
    }
  },
  babel: {
    plugins: [['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }]]
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true
            // modifyVars: defaultVars
          }
        }
      }
    }
  ],
  webpack: {
    configure: (config, { env, paths }) => {
      const newConfig = { ...config };
      const options = {
        antDir: path.join(__dirname, './node_modules/antd'),
        stylesDir: path.join(__dirname, './src/themes/styles'),
        varFile: path.join(__dirname, './src/themes/styles/variables.less'),
        mainLessFile: path.join(__dirname, './src/themes/styles/index.less'),
        themeVariables: Object.keys(defaultVars),
        indexFileName: 'index.html',
        generateOnce: false
      };
      const themePlugin = new AntDesignThemePlugin(options);
      newConfig.plugins.push(themePlugin);
      return newConfig;
    }
  }
};
