import { Fragment } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { setI18nConfig } from 'res/languages';
import MasterRoutes from 'routing/master.route';
import RouteConfigs from 'routing/config.route';
import ChangeTheme from 'libraries/components/ChangeTheme';
import useInitTheme from 'hooks/ui/theme/useInitTheme';
import './App.css';

setI18nConfig();
function App() {
    useInitTheme();

    return (
        <Fragment>
            <Router>
                <MasterRoutes routes={RouteConfigs} />
                <ChangeTheme />
            </Router>
        </Fragment>
    );
}

export default App;
