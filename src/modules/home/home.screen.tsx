import Container from 'libraries/components/container.component';
import { translate } from 'res/languages';

const HomeScreen = () => {
    return (
        <Container>
            <h1>{translate('home')}</h1>
        </Container>
    );
};

export default HomeScreen;
