
/* 
  Created by vungbt at 06-21-2021 16:15:08
  Module supermarket
*/
import { Form } from 'antd';
import { CreateDemoRouterInput } from './demo-router.types';
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { ActiveType, ParamUrl } from 'types/common.types';
import {
    createDemoRouter,
    fetchDemoRouterDetail,
    updateDemoRouter
} from './demo-router.service';
import {
    showAlertError,
    showAlertSuccess,
    showLoading
} from 'utils/common.utils';

interface FormValues {
    avatar: string;
    name: string;
    status: ActiveType;
}

interface UtilsRes {
    form: any;
    handleSubmit: (values: FormValues) => void;
    loading: boolean;
}

export default function SupermarketDetailUtils(): UtilsRes {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState<boolean>(false);
    const params: ParamUrl = useParams();
    const history = useHistory();

    useEffect(() => {
        if (params.id) {
            fetchDataDetail();
        }
    }, []);

    // fetch data khi sửa dữ liệu
    const fetchDataDetail = async () => {
        setLoading(true);
        const res = await fetchDemoRouterDetail(params.id);
        if (res) {
            form.setFieldsValue({ name: res.name, status: res.status });
        }
        setLoading(false);
    };

    // tạo dữ liệu
    const onCreate = async (body: CreateDemoRouterInput) => {
        showLoading('loading_create')
        const { status, messageKey } = await createDemoRouter(body);
        if (status) {
            showAlertSuccess('create_success')
            history.goBack();
        } else {
            showAlertError(messageKey)
        }
    };

    // sửa dữ liệu
    const onUpdate = async (body: CreateDemoRouterInput) => {
        showLoading('loading_update')
        const { status, messageKey } = await updateDemoRouter(
            params.id,
            body
        );
        if (status) {
            showAlertSuccess('update_success')
            history.goBack();
        } else {
            showAlertError(messageKey)
        }
    };

    // submit form
    const handleSubmit = (values: FormValues): void => {

        const body: CreateDemoRouterInput = {
            name: values.name,
            status: values.status
        };
        if (params.id) {
            onUpdate(body);
        } else {
            onCreate(body);
        }
    };

    return {
        form,
        handleSubmit,
        loading
    };
}


