import { ActiveType, Admin } from 'types/common.types';

export interface DemoRouter {
    _id: string;
    name: string;
    status: ActiveType;
    creator: Admin;
}


export interface DemoRouterReq {
    q?: string;
    limit?: number;
    page?: number;
}

export interface UpdateDemoRouterInput {
    name?: string;
    status?: ActiveType;
}

export interface CreateDemoRouterInput {
    name: string;
    status: ActiveType;
}
