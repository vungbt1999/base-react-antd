import { useEffect, useState } from 'react';
// import {
//     ActiveType,
// } from 'types/common.types';
import { PAGINATE_DEFAULT } from 'constants/paging-default.constants';
import {
    DemoRouter,
    DemoRouterReq
} from './demo-router.types';
import {
    fetchDemoRouter,
    // updateDemoRouter,
    // deleteDemoRouter
} from './demo-router.service';
// import { DefaultConfirmModalParams } from 'constants/common.constants';
// import { 
//     showAlertError,
//     showAlertSuccess,
//     showLoading
// } from 'utils/common.utils';


interface UtilsRes {
    dataList: DemoRouter[];
    page: number;
    dataTotal: number;
    handleOkPopup: () => void;
    onHideConfirmPopup: () => void;
    onPageChange: (page: number) => void;
    loading: boolean;
    onSearch: (value: string) => void;
    // modalParams: ConfirmModalParams;
    onShowDeletePopup: (index: number) => void;
    onShowChangeStatusPopup: (index: number) => void;
}

// tên function phải viết hoa chữ cái đầu
export default function DemoRouterUtils(): UtilsRes {
    const [dataList, setDataList] = useState<DemoRouter[]>([]);
    const [page, setPage] = useState<number>(PAGINATE_DEFAULT.PAGE);
    const [dataTotal, setDataTotal] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(true);
    // const [dataIndex, setDataIndex] = useState<number>();
    // const [modalParams, setModalParams] = useState<ConfirmModalParams>(
    //     DefaultConfirmModalParams
    // );

    useEffect(() => {
        // didmount
        fetchData({ page });
    }, []);

    // lấy dữ liệu
    const fetchData = async (params: DemoRouterReq) => {
        setLoading(true);
        const { data, total } = await fetchDemoRouter({
            ...params,
            limit: PAGINATE_DEFAULT.LIMIT
        });
        setDataList(data);
        setDataTotal(total);
        setLoading(false);
    };

    // hiển thị modal xóa
    const onShowDeletePopup = (index: number) => () => {
        // setModalParams({
        //     visible: true,
        //     title: 'DemoRouter.deleteTitle',
        //     content: 'DemoRouter.deleteContent',
        //     type: ConfirmModalType.DELETE
        // });
        // setDataIndex(index);
    };

    // hiển thị modal thay đổi trạng thái
    const onShowChangeStatusPopup = (index: number) => () => {
        // const data = dataList[index];
        // const isActive = data.status === ActiveType.ACTIVE;
        // setModalParams({
        //     visible: true,
        //     title: 'changeStatus',
        //     content: isActive ? 'changeToInActive' : 'changeToActive',
        //     type: ConfirmModalType.CHANGE_STATUS
        // });
        // setDataIndex(index);
    };

    // đóng modal
    const onHideConfirmPopup = (): void => {
        // setModalParams(DefaultConfirmModalParams);
        // setDataIndex(undefined);
    };

    // xử lý button ok của popup
    const handleOkPopup = (): void => {
        // if (modalParams.type === ConfirmModalType.DELETE) {
        //     onDeleteItem();
        // }
        // if (modalParams.type === ConfirmModalType.CHANGE_STATUS) {
        //     onChangeStatus();
        // }
    };

    // // call api xóa
    // const onDeleteItem = async (): Promise<void> => {
    //     onHideConfirmPopup();
    //     if (dataIndex === undefined) return;
    //     showLoading('loading_delete')
    //     const id = dataList[dataIndex]._id;
    //     const { messageKey, status } = await deleteDemoRouter(id);
    //     if (status) {
    //         showAlertSuccess('delete_success')
    //         const data = dataList.filter((e) => e._id !== id);
    //         setDataList(data);
    //         fetchData({ page });
    //     } else {
    //         showAlertError(messageKey)
    //     }
    // };


    // // thay đổi status
    // const onChangeStatus = async (): Promise<void> => {
    //     onHideConfirmPopup();
    //     if (dataIndex === undefined) return;
    //     const data = [...dataList];
    //     const status =
    //         data[dataIndex].status === ActiveType.ACTIVE
    //             ? ActiveType.INACTIVE
    //             : ActiveType.ACTIVE;
    //     const { _id } = data[dataIndex];
    //     const res = await updateDemoRouter(_id, { status });
    //     if (res && res.status) {
    //         data[dataIndex].status = status;
    //         setDataList(data);
    //     } else {
    //         showAlertError(res.messageKey)
    //     }
    // };

    // chuyển page
    const onPageChange = (page: number): void => {
        setPage(page);
        fetchData({ page });
    };

    const onSearch = (value: string): void => {
        setPage(PAGINATE_DEFAULT.PAGE);
        fetchData({ page: PAGINATE_DEFAULT.PAGE, q: value });
    };

    return {
        dataList,
        page,
        handleOkPopup,
        onShowDeletePopup,
        onShowChangeStatusPopup,
        loading,
        onPageChange,
        dataTotal,
        onSearch,
        onHideConfirmPopup
    };
}



