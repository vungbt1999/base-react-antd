import {
    DemoRouter,
    DemoRouterReq,
    CreateDemoRouterInput,
    UpdateDemoRouterInput
} from './demo-router.types';
import { MessageAndStatusRes } from 'types/common.types';
import { ResponseBase, ResponseCode } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';

const apiName = {
    DemoRouter: 'cc-buy-more'
};

export const fetchDemoRouter = async (
    params?: DemoRouterReq
): Promise<{ data: DemoRouter[]; total: number }> => {
    const res = await ApiUtils.fetch<
        DemoRouterReq,
        ResponseBase<DemoRouter[]>
    >(apiName.DemoRouter, params);

    let data: DemoRouter[] = [];
    let total = 0;

    if (res && res.status === ResponseCode.SUCCESS) {
        data = res.data!;
        total = res.total!;
    }
    return { data, total };
};

export const createDemoRouter = async (
    params?: CreateDemoRouterInput
): Promise<MessageAndStatusRes> => {
    const res = await ApiUtils.post<
        CreateDemoRouterInput,
        ResponseBase<DemoRouter>
    >(apiName.DemoRouter, params);

    let messageKey = 'create_failure';

    switch (res && res.status) {
        default:
            break;
    }

    return {
        status: res.status === ResponseCode.SUCCESS,
        messageKey
    };
};

export const updateDemoRouter = async (
    _id: string,
    params?: UpdateDemoRouterInput
): Promise<MessageAndStatusRes> => {
    const res = await ApiUtils.put<
        UpdateDemoRouterInput,
        ResponseBase<DemoRouter>
    >(`${apiName.DemoRouter}/${_id}`, params);

    let messageKey = 'update_failure';

    switch (res && res.status) {
        default:
            break;
    }

    return {
        status: res.status === ResponseCode.SUCCESS,
        messageKey
    };
};

export const deleteDemoRouter = async (
    _id: string
): Promise<MessageAndStatusRes> => {
    const res = await ApiUtils.remove<undefined, ResponseBase<boolean>>(
        `${apiName.DemoRouter}/${_id}`,
        undefined,
        { isAuth: true }
    );

    let messageKey = 'delete_failure';

    if (res && res.status) {
        messageKey = '';
    }

    return { status: res.status === ResponseCode.SUCCESS, messageKey };
};

export const fetchDemoRouterDetail = async (
    _id: string
): Promise<DemoRouter | undefined> => {
    const res = await ApiUtils.fetch<undefined, ResponseBase<DemoRouter>>(
        `${apiName.DemoRouter}/${_id}`,
        undefined,
        { isAuth: true }
    );

    if (res && res.status === ResponseCode.SUCCESS) {
        return res.data;
    }
    return undefined;
};


