import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Tooltip, Typography } from 'antd';
import { PAGINATE_DEFAULT } from 'constants/paging-default.constants';
import { useHistory } from 'react-router-dom';
import { translate } from 'res/languages';
import { DemoRouter } from './demo-router.types';

interface ColumnParams {
    page: number;
    onChangeStatus: (index: number) => any;
    onShowDeletePopup: (index: number) => any;
}

const DemoRouterColumns = ({
    page,
    onChangeStatus,
    onShowDeletePopup
}: ColumnParams) => {
    const history = useHistory();

    const onEdit = (id: string) => {
        history.push(`/demo-router/${id}`);
    };

    return [
        {
            title: translate('stt'),
            dataIndex: 'index',
            key: 'index',
            width: '2%',
            render: (_: any, __: any, index: number) => {
                return (
                    <span>
                        {Number(page) > 1
                            ? (Number(page) - 1) * PAGINATE_DEFAULT.LIMIT +
                              (index + 1)
                            : index + 1}
                    </span>
                );
            }
        },
        {
            title: translate('demoRouter.name'),
            key: 'title',
            render: (_: any, item: DemoRouter) => {
                return (
                    <Typography.Text >
                        {item.name}
                    </Typography.Text>
                );
            },
            sorter: (a: any, b: any) =>
                (a?.title ?? '').localeCompare(b?.title ?? '', 'vi', {
                    sensitivity: 'base'
                }),
            showSorterTooltip: false,
            width: '40%'
        },
        {
            title: translate('action'),
            dataIndex: 'action',
            key: 'action',
            width: '10%',
            render: (_: string, item: DemoRouter, index: number) => {
                return (
                    <div>
                        <Tooltip placement="top" title={translate('edit')}>
                            <EditOutlined className="iconEdit" onClick={() => onEdit(item?._id)}/>
                        </Tooltip>
                        <Tooltip placement="top" title={translate('delete')}>
                            <DeleteOutlined className="iconDelete" onClick={onShowDeletePopup(index)}/>
                        </Tooltip>
                    </div>
                );
            }
        }
    ];
};

export default DemoRouterColumns;


