import Container from 'libraries/components/container.component';
import { Form } from 'antd';
import BaseFormActions from 'libraries/form/base-form-actions';
import DemoRouterDetailUtils from './utils/demo-router-detail.utils';
import PageLoading from 'libraries/loading/loading.libraries';
import NameComponent from 'libraries/input-name/name.libraries';

export default function DemoRouterDetailScreen () {
    const { form, handleSubmit, loading } = DemoRouterDetailUtils();

    return (
        <Container>
            {loading && <PageLoading />}
            {!loading && (
                <Form form={form} onFinish={handleSubmit}>
                    <NameComponent />
                    <BaseFormActions />
                </Form>
            )}
        </Container>
    )
}