import Container from 'libraries/components/container.component';
import { Col, Row, Table } from 'antd';
import AddButton from 'libraries/button/add-button.libraries';
import BoxSearch from 'libraries/box-search/box-search';
import DemoRouterColumns from './utils/demo-router.columns';
import { translate } from 'res/languages';
import BasePagination from 'libraries/pagination/pagination.libraries';
import DemoRouterUtils from './utils/demo-router.utils';

export default function DemoRouterDetailScreen() {
    const {
        dataList,
        page,
        dataTotal,
        // handleOkPopup,
        onShowDeletePopup,
        // onHideConfirmPopup,
        loading,
        onPageChange,
        onSearch,
        onShowChangeStatusPopup
    } = DemoRouterUtils();

    return (
        <Container
            renderRight={AddButton({
                title: 'demoRouter.textAdd',
                path: '/demo-router/add'
            })}
        >
            <Row>
                <Col span={6} offset={0}>
                    <BoxSearch
                        onSearch={onSearch}
                        placeHolder={translate('demoRouter.textAdd')}
                    />
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <Table
                        pagination={false}
                        loading={loading}
                        style={{ marginTop: 20 }}
                        dataSource={dataList}
                        columns={DemoRouterColumns({
                            page,
                            onChangeStatus: onShowChangeStatusPopup,
                            onShowDeletePopup
                        })}
                        rowKey="_id"
                    />
                </Col>

                <BasePagination
                    page={page}
                    total={dataTotal}
                    onPageChange={onPageChange}
                />
            </Row>
            {/* <ConfirmModal
                handleCancel={onHideConfirmPopup}
                handleOk={handleOkPopup}
                visible={modalParams.visible}
                title={modalParams.title}
                content={modalParams.content}
            /> */}
        </Container>
    )
}