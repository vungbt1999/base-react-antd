import dark from 'themes/dark';
import defaultTheme from 'themes/default';

const themes = { dark, default: defaultTheme };
export default themes;