import theme from '../ui/theme/reducer';

const baseReducer = { theme };

export default baseReducer;
