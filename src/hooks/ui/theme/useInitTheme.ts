import { useEffect } from 'react';
import themes from 'themes';
import useTheme from './useTheme';
import { AsyncStorageUtils, StorageKey } from 'utils/async-storage.utils';

function useInitTheme() {
  const { theme } = useTheme();

  useEffect(() => {
    const newVariables = { ...theme.variables };
    const configTheme = {
      ...themes[theme.name],
      ...newVariables
    };
    global.less
      .modifyVars(configTheme)
      .then(() => {
        AsyncStorageUtils.saveObject(StorageKey.THEME, theme);
        window.document.body.dataset.theme = theme.name;
      })
      .catch((err: any) => console.log(err));
  }, [theme]);
}

export default useInitTheme;
