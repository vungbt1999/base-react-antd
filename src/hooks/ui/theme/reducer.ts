import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';
import variables from 'themes/variables';
import { AsyncStorageUtils, StorageKey } from 'utils/async-storage.utils';

export interface ThemeState {
  name: 'default' | 'dark';
  variables: {
    [key: string]: string;
  };
}

const themeLocal = AsyncStorageUtils.getObject(StorageKey.THEME);
console.log(themeLocal);
const initialState: ThemeState = {
  name: themeLocal.name || 'default',
  variables: Object.keys(themeLocal.variables || {}).length
    ? themeLocal.variables
    : { '@primary-color': variables['@primary-color'] }
};

const theme = createSlice({
  name: 'theme',
  initialState: initialState,
  reducers: {
    changeTheme: (
      state: ThemeState,
      action: PayloadAction<{
        name?: ThemeState['name'];
        variables?: {
          [key: string]: string;
        };
      }>
    ) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = theme;
export const { changeTheme } = actions;
export default reducer;
