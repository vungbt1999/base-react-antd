export enum MediaType {
    PHOTO = 'PHOTO',
    VIDEO = 'VIDEO',
    FILE = 'FILE',
    AUDIO = 'AUDIO'
}

export interface Media {
    _id: string;
    type?: MediaType;
    url: string;
    name?: string;
}
