export enum ActiveType {
    ACTIVE = 'ACTIVE',
    INACTIVE = 'INACTIVE'
}

export enum AdminType {
    STAFF = 'STAFF',
    ADMIN = 'ADMIN'
}

export interface Admin {
    _id: string;
    name: string;
    dialCode: string;
    phone: string;
    position: string;
    email: string;
    birthday: string;
    gender: string;
    createdAt: string;
    status: ActiveType;
    role: AdminType;
}

export interface MessageAndStatusRes {
    messageKey: string;
    status: boolean;
}

export interface ParamUrl {
    id: string;
}