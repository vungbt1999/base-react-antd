import { configureStore } from '@reduxjs/toolkit';
import baseReducer from 'hooks/reducer/reducer';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';

const rootReducer = {
  ...baseReducer
};

const store = configureStore({
  reducer: rootReducer,
  middleware: [
    createStateSyncMiddleware({
      whitelist: ['theme/changeTheme']
    })
  ]
});
initMessageListener(store);

export type RootState = ReturnType<typeof store.getState>;
export default store;
