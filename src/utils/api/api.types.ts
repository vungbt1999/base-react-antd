export enum ResponseCode {
    SUCCESS = 'SUCCESS'
}

export interface ResponseBase<T> {
    status: string;
    data?: T;
    total?: number;
    statusCode?: number;
    message?: string;
}
