import axios, { AxiosRequestConfig, ResponseType , AxiosResponse} from 'axios';
import { AsyncStorageUtils, StorageKey } from '../async-storage.utils';
import { showAlertError } from 'utils/common.utils';

interface CustomHeaders {
    isAuth: boolean;
}

const REQ_TIMEOUT = 25 * 1000;
export const __DEV__ =
    !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const instance = axios.create({
    baseURL: process.env.REACT_APP_API_DOMAIN,
    timeout: REQ_TIMEOUT
});

instance.interceptors.request.use((_config) => requestHandler(_config));

const requestHandler = (request: AxiosRequestConfig) => {
    if (__DEV__) {
        console.log(
            `Request API: ${request.url}`,
            request.params,
            request.data
        );
    }

    return request;
};

instance.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
);

const errorHandler = (error: any) => {
    if (__DEV__) {
        console.log(error, '123');
    }
    return Promise.reject({ ...error });
};

const successHandler = async (response: AxiosResponse) => {
    if (__DEV__) {
        console.log(`Response API: ${response.config.url}`, response.data);
    }

    const data: any = response.data;
    if (!data || data.status === 'INVALID_TOKEN') {
        showAlertError('login_expired')
        // EventBus.getInstance().post({
        //     type: EventBusName.LOGOUT_EVENT
        // });
        return;
    }
    return data;
};

async function fetch<ReqType, ResType>(
    url: string,
    params?: ReqType,
    customHeaders?: CustomHeaders,
    responseType?: ResponseType
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers, responseType });
}

async function post<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, { ...data }, { headers });
}

async function postForm<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers });
}

async function put<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.put(url, { ...data }, { headers });
}

async function remove<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { data: { ...data }, headers: { ...headers } });
}

function getHeader(customHeaders?: CustomHeaders): any {
    const header: any = customHeaders || {};
    const token = AsyncStorageUtils.get(StorageKey.TOKEN);
    header.Authorization = `Bearer ${token}`;

    return { ...header };
}

const ApiUtils = { fetch, post, put, postForm, remove };
export default ApiUtils;
