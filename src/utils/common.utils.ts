import { translate } from '../res/languages';
import { message } from 'antd';
import { AsyncStorageUtils, StorageKey } from './async-storage.utils';
import { Admin, AdminType} from '../types/common.types';


export const checkAdmin = (): boolean => {
    const user: Admin | null = AsyncStorageUtils.getObject(StorageKey.USER);
    if (!user) return false;
    return user.role === AdminType.ADMIN;
};

const imageMime = ['image/jpeg', 'image/png', 'image/jpg'];

export const isImageFile = (type: string): boolean => {
    return imageMime.includes(type);
};

const videoMime = [
    'video/mp4',
    'video/x-msvideo',
    'video/mpeg',
    'video/ogg',
    'video/webm',
    'video/3gpp',
    'video/quicktime'
];

const audioMime = [
    'audio/3gpp',
    'audio/aac',
    'audio/ogg',
    'audio/opus',
    'audio/wav',
    'audio/mp3',
    'audio/mpeg'
];

export const isAudio = (type: string): boolean => {
    return audioMime.includes(type);
};

export const isVideo = (type: string): boolean => {
    return videoMime.includes(type);
};

export const isVideoOrAudio = (type: string): boolean => {
    return isVideo(type) || isAudio(type);
};

export const showLoading = (content: string, key?: string): void => {
    message.loading({
        content: translate(content),
        key: key || 'helper'
    });
};

export const showAlertSuccess = (content: string, key?: string): void => {
    message.success({
        content: translate(content),
        key: key || 'helper'
    });
};

export const showAlertError = (content: string, key?: string): void => {
    message.error({
        content: translate(content),
        key: key || 'helper'
    });
};

export const isEmpty = (text: string | number): boolean => {
    if (text === undefined || text === null || typeof text === 'object') return true;
    if (typeof text === 'string' && text.trim() === '') return true;

    return false;
};