import { FileDoneOutlined } from '@ant-design/icons';
import { lazy } from 'react';
import { IRoute } from 'routing/route.types';

const DemoRouterScreen = lazy(
    () => import('modules/demo-router/demo-router.screen')
);

const DemoRouterDetailScreen = lazy(
    () => import('modules/demo-router/demo-router-detail.screen')
);

const ContentLayout = lazy(
    () => import('libraries/layouts/content.layout')
);

const DemoRouterRoute: IRoute = {
    path: '/demo-router',
    name: 'demoRouter.menu',
    component: ContentLayout,
    icon: <FileDoneOutlined />,
    routes: [
        {
            path: '/demo-router',
            name: 'demoRouter.title',
            component: DemoRouterScreen,
            exact: true
        },
        {
            path: '/demo-router/add',
            name: 'demoRouter.textAdd',
            component: DemoRouterDetailScreen,
            hideInMenu: true,
            exact: true
        },
        {
            path: '/demo-router/:id',
            name: 'demoRouter.textDetail',
            component: DemoRouterDetailScreen,
            hideInMenu: true,
            exact: true
        },
    ]
};

export default DemoRouterRoute;




