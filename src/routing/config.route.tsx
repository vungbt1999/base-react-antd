import { lazy } from 'react';
import { IRoute } from './route.types';
import { HomeOutlined } from '@ant-design/icons';
import DemoRouteRoute from './routes/demo-router';

const MasterLayout = lazy(() => import('libraries/layouts/master.layout'));
const HomeScreen = lazy(() => import('modules/home/home.screen'));

const RouteConfigs: IRoute[] = [
    {
        path: '/',
        name: 'home',
        component: MasterLayout,
        hideInMenu: true,
        routes: [
            {
                path: '/',
                name: 'home',
                icon: <HomeOutlined />,
                exact: true,
                component: HomeScreen
            },
            DemoRouteRoute
        ]
    }
];

export default RouteConfigs;
