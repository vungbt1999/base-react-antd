import React, { ComponentType, LazyExoticComponent } from 'react';

export interface IRoute {
    name: string;
    icon?: any;
    path: string;
    hideInMenu?: boolean;
    existSubMenu?: boolean;
    component: LazyExoticComponent<ComponentType<any>>;
    redirect?: string;
    authority?: Array<any>;
    exact?: boolean;
    routes?: Array<IRoute>;
    componentFallback?: React.FC;
}

export interface IMasterRouter {
    routes: Array<IRoute>;
}
