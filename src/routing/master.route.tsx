import { Spin } from 'antd';
import React, { ComponentType, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { IMasterRouter, IRoute } from './route.types';

const MasterRoutes = (props: IMasterRouter) => {
    const routes = props.routes;

    return (
        <Switch>
            {routes.map((route: IRoute, index: number) => {
                return (
                    <Route
                        path={route.path}
                        exact={route.exact}
                        component={() =>
                            WaitingComponent(route.component, route.routes, route.componentFallback)
                        }
                        key={index}
                    />
                );
            })}
            <Redirect to="/" />
        </Switch>
    );
};

const WaitingComponent = (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Component: React.LazyExoticComponent<ComponentType<any>>,
    routes: Array<IRoute> = [],
    ComponnetFallback?: React.FC
) => {
    return (
        <Suspense
            fallback={
                ComponnetFallback ? (
                    <ComponnetFallback />
                ) : (
                    <div
                        style={{
                            display: 'flex',
                            flex: 1,
                            marginTop: 100,
                            alignContent: 'center',
                            justifyContent: 'center',
                        }}>
                        <Spin />
                    </div>
                )
            }>
            <Component routes={routes} />
        </Suspense>
    );
};

export default MasterRoutes;
