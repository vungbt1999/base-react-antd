
import { Form, Input } from 'antd';
import { translate } from 'res/languages';

interface Props {
    name?: string;
    isRequired?: boolean;
};

export default function NameComponent({name, isRequired = true }: Props) {
    return (
        <>
            <div className="titleRequired">
                {translate('demoRouter.name')}
            </div>
            <Form.Item
                // messageVariables={"labl"}
                name={name ? name : 'name'}
                labelCol={{ span: 7 }}
                labelAlign="left"
                rules={[
                    {
                        whitespace: isRequired,
                        required: isRequired,
                        message: translate('requiredField', {label: translate('demoRouter.name')})
                    }
                ]}
            >
                <Input size="large" />
            </Form.Item>
        </>
    );
}

