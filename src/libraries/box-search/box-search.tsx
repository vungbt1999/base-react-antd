import { Input } from 'antd';
import { ChangeEvent, useEffect, useState } from 'react';
import { useDebounceFn } from 'ahooks';
import styles from './styles.module.scss';
import { SearchOutlined } from '@ant-design/icons';

interface Props {
    onSearch: (value: string) => void;
    timeDelay?: number;
    placeHolder?: string;
    className?: string;
}

function BoxSearch(props: Props) {
    const { timeDelay, onSearch, placeHolder, className } = props;

    const [search, setSearch] = useState<string>('');

    const { run } = useDebounceFn(
        (value: string) => {
            setSearch(value);
        },
        {
            wait: timeDelay || 600
        }
    );

    useEffect(() => {
        onSearch(search);
    }, [search]);

    return (
        <Input
            className={'borderRadius' || className}
            allowClear
            placeholder={placeHolder}
            size="large"
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
                run(e.target.value)
            }
            suffix={
                <SearchOutlined className={styles.iconSearch}/>
            }
        />
    );
}

export default BoxSearch;
