import { LogoutOutlined, MailOutlined } from '@ant-design/icons';
import { Menu, Modal } from 'antd';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { translate } from 'res/languages';
import { IMasterRouter } from 'routing/route.types';
import { AsyncStorageUtils } from 'utils/async-storage.utils';

const { SubMenu } = Menu;

const MenuComponent = (props: IMasterRouter) => {
    const history = useHistory();
    const index = props.routes.findIndex((route) => route.path === '/');
    const routes = props.routes[index].routes!;
    
    const getDefaultMenu = () => {
        const currentPath = window.location.pathname;
        let key = '';
        let open = '';
        for (let index = 0; index < routes.length; index++) {
            const route = routes[index];
            if (route.routes && route.routes.length > 0 && route.existSubMenu) {
                const childRoute = route.routes.find((it) => it.path === currentPath);
                if (childRoute) {
                    key = childRoute.name;
                    open = route.name;
                }
            } else {
                if (route.path === currentPath) {
                    key = route.name;
                    break;
                }
            }
        }
        return {
            selected: key,
            open
        };
    };

    const defaultMenu = getDefaultMenu();

    const [openKeys, setOpenKeys] = React.useState<string[]>([defaultMenu.open]);

    const onSelect = (info: any) => {
        if (info.key === 'signOut') {
            Modal.warning({
                title: translate('signOut'),
                content: translate('confirmSignOut'),
                okText: translate('signOut'),
                cancelText: translate('cancel'),
                onOk: () => {
                    AsyncStorageUtils.clear();
                    history.push('./auth/login');
                },
                okCancel: true,
            })
            return;
        }
        let path;
        for (let index = 0; index < routes.length; index++) {
            const route = routes[index];
            if (route.routes && route.routes.length > 0 && route.existSubMenu) {
                const childRoute = route.routes.find((it) => it.name === info.key);
                if (childRoute) path = childRoute.path;
            } else {
                if (route.name === info.key) {
                    path = route.path;
                    break;
                }
            }
        }

        if (path) {
            history.push(path);
        }
    };

    const onOpenChange = (keys: any) => {
        const latestOpenKey = keys.find((key: any) => openKeys.indexOf(key) === -1);

        const exists = routes.some((route) => route.name === latestOpenKey);
        if (!exists) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };

    return (
        <Menu
            onSelect={onSelect}
            // theme="dark"
            mode="inline"
            selectedKeys={[defaultMenu.selected]}
            openKeys={openKeys}
            onOpenChange={onOpenChange}
            defaultSelectedKeys={[defaultMenu.selected]}>
            {routes.map((route) => {
                if (route.hideInMenu) return null;
                if (route.routes && route.routes.length > 0 && route.existSubMenu) {
                    return (
                        <SubMenu
                            key={route.name}
                            icon={route.icon || <MailOutlined />}
                            title={translate(route.name)}>
                            {route.routes.map((childRoute) => {
                                if (childRoute.hideInMenu) return null;
                                return (
                                    <Menu.Item key={childRoute.name}>
                                        {translate(childRoute.name)}
                                    </Menu.Item>
                                );
                            })}
                        </SubMenu>
                    );
                }

                return (
                    <Menu.Item icon={route.icon} key={route.name}>
                        {translate(route.name)}
                    </Menu.Item>
                );
            })}
            <Menu.Item icon={<LogoutOutlined />} key={'signOut'}>
                {translate('signOut')}
            </Menu.Item>
        </Menu>
    );
};

export default MenuComponent;
