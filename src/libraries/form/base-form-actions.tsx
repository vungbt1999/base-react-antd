import { Col, Row, Form, Button } from 'antd';
import { useHistory } from 'react-router-dom';
import { translate } from 'res/languages';

interface Props {
    hideCancel?: boolean;
}

export default function BaseFormActions({ hideCancel }: Props) {
    const history = useHistory();
    const closeButton = () => {
        history.goBack();
    };

    return (
        <Row gutter={10}>
            {!hideCancel && (
                <Col>
                    <Form.Item>
                        <Button
                            size="large"
                            type="primary"
                            style={{
                                background: '#F34949',
                                borderRadius: '3px',
                                border: 'none',
                                width: '100px'
                            }}
                            onClick={closeButton}
                        >
                            {translate('cancel')}
                        </Button>
                    </Form.Item>
                </Col>
            )}
            <Col>
                <Form.Item>
                    <Button
                        size="large"
                        type="primary"
                        htmlType="submit"
                        style={{
                            background: '#3AC86E',
                            borderRadius: '3px',
                            border: 'none',
                            width: '100px'
                        }}
                    >
                        {translate('save')}
                    </Button>
                </Form.Item>
            </Col>
        </Row>
    );
}
