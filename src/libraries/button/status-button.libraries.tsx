import { translate } from 'res/languages';
import { ActiveType } from 'types/common.types';

interface Props {
    onChangeStatus?: () => void;
    status: ActiveType;
    // itemId: string;
}

const StatusKey = {
    [ActiveType.ACTIVE]: 'active',
    [ActiveType.INACTIVE]: 'inactive',
};


export default function StatusButton({ status, onChangeStatus }: Props) {
    return (
        <div
            onClick={onChangeStatus}
            className={`table-status-${StatusKey[status]}`}
        >
            {translate(StatusKey[status])}
        </div>
    )
}