import { PlusOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { useHistory } from 'react-router-dom';
import { translate } from 'res/languages';
 
interface Props {
    title: string;
    path: string;
}

function AddButton({ title, path }: Props) {
    const history = useHistory();
    const onAddClicked = () => {
        history.push(path);
    };

    return (
        <Button
            onClick={onAddClicked}
            icon={<PlusOutlined />}
            type="primary"
            size="large"
        >
            {translate(title)}
        </Button>
    );
}

export default AddButton;
