import HeaderComponent from './header.component';

interface Props {
    children: any;
    renderRight?: JSX.Element;
    hideHeader?: boolean;
    showShadown?: boolean;
}
const Container = (props: Props) => {
    return (
        <div
            style={{
                flex: 1,
                display: 'flex',
                flexDirection: 'column',
                borderRadius: 6,
                minHeight: '85vh'
            }}>
            {!props.hideHeader && <HeaderComponent renderRight={props.renderRight} />}
            <div
                style={{
                    margin: 20,
                    padding: 20,
                    borderRadius: 6,
                    boxShadow: props.showShadown
                        ? '0 3px 7px 0 rgba(112, 112, 112, 0.1), 0 3px 7px 0 rgba(112, 112, 112, 0.1)'
                        : '0px'
                }}>
                {props.children}
            </div>
        </div>
    );
};

export default Container;
