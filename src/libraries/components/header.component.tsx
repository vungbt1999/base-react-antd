import { Breadcrumb, Typography } from 'antd';
import React from 'react';
import { translate } from 'res/languages';
import RouteConfigs from 'routing/config.route';
import { TextUtils } from 'utils/TextUtils';

interface Props {
    renderRight?: JSX.Element;
}

const HeaderComponent = (props: Props) => {
    const index = RouteConfigs.findIndex((route) => route.path === '/');
    const routes = RouteConfigs[index].routes;
    if (!routes) return null;

    const getDefaultMenu = () => {
        const currentPath = TextUtils.transformIdPath(window.location.pathname);

        let parentRoute = undefined;
        let subRoute = undefined;

        for (let index = 0; index < routes.length; index++) {
            const route = routes[index];
            if (route.routes && route.routes.length > 0) {
                const childRoute = route.routes.find((it) => {
                    return it.path === currentPath;
                });
                if (childRoute) {
                    parentRoute = route;
                    subRoute = childRoute;
                    break;
                }
            } else {
                if (route.path === currentPath) {
                    parentRoute = route;
                    break;
                }
            }
        }

        if (!!subRoute) {
            if (subRoute.path === parentRoute?.path) {
                subRoute = undefined;
            }
        }

        return {
            parentRoute,
            subRoute
        };
    };

    const { parentRoute, subRoute } = getDefaultMenu();

    const label = !!subRoute ? subRoute.name : parentRoute?.name;

    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '10px 24px 10px 24px',
            }}>
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item>{translate(parentRoute?.name)}</Breadcrumb.Item>
                    {!!subRoute && (
                        <Breadcrumb.Item>
                            <a href={subRoute.path}>{translate(subRoute?.name)}</a>
                        </Breadcrumb.Item>
                    )}
                </Breadcrumb>
                <Typography.Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {translate(label)}
                </Typography.Text>
            </div>

            {props.renderRight}
        </div>
    );
};

export default React.memo(HeaderComponent);
