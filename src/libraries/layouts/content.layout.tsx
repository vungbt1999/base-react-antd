import { Suspense } from 'react';
import MasterRoutes from 'routing/master.route';
import { IRoute } from 'routing/route.types';

export interface IMasterLayoutProps {
    routes: Array<IRoute>;
}

const ContentLayout = (props: IMasterLayoutProps) => {
    return (
        <Suspense fallback="">
            <div style={{}}>
                <MasterRoutes routes={props.routes} />
            </div>
        </Suspense>
    );
};

export default ContentLayout;
