import { UserOutlined } from '@ant-design/icons';
import { Avatar, Button, Layout } from 'antd';
import MenuComponent from 'libraries/menu/menu.layout';
import { Suspense } from 'react';
import { useHistory } from 'react-router-dom';
import MasterRoutes from 'routing/master.route';
import RouteConfigs from 'routing/config.route';
import { IRoute } from 'routing/route.types';
import { isEnglish, setI18nConfig } from 'res/languages';

const { Header, Content, Footer, Sider } = Layout;

export interface IMasterLayoutProps {
    routes: Array<IRoute>;
}

const MainLayout = (props: IMasterLayoutProps) => {
    const history = useHistory();
    const isEn = isEnglish();

    const onChangeLanguage = (locale: string) => () => {
        setI18nConfig(locale);
        history.push('/');
    };

    return (
        <Suspense fallback="">
            <Layout>
                <Sider
                    style={{
                        overflow: 'auto',
                        height: '100vh',
                        position: 'fixed',
                        left: 0
                    }}>
                    <div className="logoCms">
                        <img src="/reactjs.svg" alt="reactjs"/>
                        <span>+</span>
                        <img src="/antd.svg" alt="antd"/>
                    </div>

                    <MenuComponent routes={RouteConfigs} />
                </Sider>
                <Layout style={{ marginLeft: 200 }}>
                    <Header
                        style={{
                            padding: 0,
                            height: 60,
                            zIndex: 1,
                            display: 'flex',
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                            paddingRight: 20,
                            boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.1)'
                        }}>

                        <Button
                            type={isEn ? 'ghost' : undefined}
                            onClick={onChangeLanguage('en')}>
                            En
                        </Button>
                        <Button
                            type={!isEn ? 'ghost' : undefined}
                            onClick={onChangeLanguage('vi')}>
                            Vi
                        </Button>
                        <Avatar style={{ backgroundColor: '#87d068' , marginLeft: 10}} icon={<UserOutlined />} />
                    </Header>
                    <Content style={{minHeight: 'auto'}}>
                        <MasterRoutes routes={props.routes} />
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        <p>
                            &copy; repo{' '}
                            <a
                                href="https://gitlab.com/vungbt1999"
                                target="_blank"
                                rel="noreferrer">
                                vungbt1999
                            </a>
                        </p>
                    </Footer>
                </Layout>
            </Layout>
        </Suspense>
    );
};

export default MainLayout;
