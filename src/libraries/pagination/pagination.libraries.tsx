import { Col, Pagination } from 'antd';
import { PAGINATE_DEFAULT } from 'constants/paging-default.constants';
import React from 'react';

interface Props {
    page: number;
    total: number;
    onPageChange: (page: number) => void;
}

export default class BasePagination extends React.PureComponent<Props> {
    render() {
        const { page, total, onPageChange } = this.props;
        return (
            <Col
                span={24}
                style={{
                    display: 'inline-flex',
                    justifyContent: 'flex-end',
                    marginTop: '10px'
                }}
            >
                <div className="d-flex justify-content-end pagination">
                    <Pagination
                        size="small"
                        showSizeChanger={false}
                        total={total}
                        showTotal={(total, range) =>
                            `${range[0]}-${range[1]}/${total}`
                        }
                        current={page}
                        pageSize={PAGINATE_DEFAULT.LIMIT}
                        defaultPageSize={PAGINATE_DEFAULT.LIMIT}
                        onChange={onPageChange}
                    />
                </div>
            </Col>
        );
    }
}
